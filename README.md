# HED-UNet

Code for HED-UNet, a model for simultaneous semantic segmentation and edge detection.

## Usage

In order to use this for your project, you will need adapt either the `get_dataloader` function in `train.py` or the methods in `data_loading.py`.
